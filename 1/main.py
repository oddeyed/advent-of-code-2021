
def part_1():
    with open('input_1.txt') as f:
        acc = 0
        cur = int(f.readline())
        line = f.readline()
        while line:
            num = int(line)
            if num > cur:
                acc += 1
            cur = num
            line = f.readline()
        print(acc)


def part_2():
    with open('input_1.txt') as f:
        numbers = [int(l) for l in f]

    window_sums = []
    for i in range(len(numbers)):
        window = numbers[i:i+3]
        if len(window) == 3:
            window_sums.append(sum(window))

    cur = window_sums[0]
    acc = 0
    for w in window_sums[1:]:
        if w > cur:
            acc += 1
        cur = w

    print(acc)

if __name__ == "__main__":
    part_1()
    part_2()
