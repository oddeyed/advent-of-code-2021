
def load_map():
    with open('input.txt') as f:
        return [[int(h) for h in row] for row in (row.strip() for row in f if row.strip())]


class Map:

    def __init__(self, height):
        self.height = height
        self.xlen = len(self.height[0])
        self.ylen = len(self.height)

    def neighbours(self, x, y):
        neighbours = [
            (x - 1, y),
            (x + 1, y),
            (x, y + 1),
            (x, y - 1)
        ]
        to_remove = set()
        for n in neighbours:
            if n[0] == -1 or n[1] == -1:
                to_remove.add(n)
            if n[0] == self.xlen or n[1] == self.ylen:
                to_remove.add(n)
        for r in to_remove:
            neighbours.remove(r)
        return neighbours

    def at(self, x, y):
        return self.height[y][x]

    def risk(self, x, y):
        return 1 + self.at(x, y)

    def iter(self):
        for x in range(0, self.xlen):
            for y in range(0, self.ylen):
                yield (x, y)

    def basin_size(self, x, y):
        assert self.at(x, y) != 9
        to_explore = set()
        explored = set()
        to_explore.add((x, y))

        while len(to_explore) > 0:
            print('to_explore', to_explore)
            print('explored', explored)
            pending = set()
            for e in to_explore:
                for n in self.neighbours(*e):
                    if self.at(*n) < 9:
                        pending.add(n)
                explored.add(e)
            to_explore.update(pending)
            to_explore -= explored

        print(explored)
        return len(explored)


def part_1():
    m = Map(load_map())
    lows = [coord for coord in m.iter()
            if all(m.at(*n) > m.at(*coord)
                   for n in m.neighbours(*coord))]
    print('lows = {} / {}'.format(len(lows), m.xlen * m.ylen))
    print('risk sum:', sum(m.risk(*coord) for coord in lows))


def part_2():
    m = Map(load_map())
    lows = [coord for coord in m.iter()
            if all(m.at(*n) > m.at(*coord)
                   for n in m.neighbours(*coord))]
    bsz = []
    for l in lows:
        bsz.append(m.basin_size(*l))
        print('basin size:', bsz[-1])

    bsz.sort()
    print('product of top 3:', bsz[-1] * bsz[-2] * bsz[-3])


if __name__ == '__main__':
    part_1()
    part_2()
