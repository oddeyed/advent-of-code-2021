
class Sub:

    def __init__(self):
        self.depth = 0
        self.horiz = 0

    def update(self, cmd: str):
        direction, amount = self.parse(cmd)
        if direction == 'forward':
            self.horiz += amount
        elif direction == 'up':
            self.depth -= amount
        elif direction == 'down':
            self.depth += amount
        else:
            raise ValueError

    @staticmethod
    def parse(cmd):
        direction, amount_str = cmd.split()
        return direction, int(amount_str)


class Sub2(Sub):

    def __init__(self):
        super().__init__()
        self.aim = 0

    def update(self, cmd: str):
        direction, amount = self.parse(cmd)
        if direction == 'down':
            self.aim += amount
        elif direction == 'up':
            self.aim -= amount
        elif direction == 'forward':
            self.horiz += amount
            self.depth += self.aim * amount


def part_1():
    s = Sub()
    with open('input.txt') as f:
        for line in f:
            s.update(line)

    print(s.horiz * s.depth)


def part_2():
    s = Sub2()
    with open('input.txt') as f:
        for line in f:
            s.update(line)

    print(s.horiz * s.depth)


if __name__ == '__main__':
    part_1()
    part_2()
