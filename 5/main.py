import re
from typing import List, Tuple


class Line:

    def __init__(self, start: Tuple, end: Tuple):
        self.start = start
        self.end = end

    def ortho(self):
        return (self.start[0] == self.end[0]) or (self.start[1] == self.end[1])

    def points(self):
        start = min(self.start, self.end)
        end = max(self.start, self.end)
        diff = [e - s for e, s in zip(end, start)]
        linelen = max(diff)
        direction = [x // linelen for x in diff]
        return [
            (start[0] + direction[0] * i, start[1] + direction[1] * i)
            for i in range(linelen + 1)
        ]

    def __repr__(self):
        return 'Line(({}, {}), ({}, {}))'.format(
            self.start[0], self.start[1],
            self.end[0], self.end[1]
        )


class HydrothermalGrid:

    def __init__(self, lines: List[Line], ortho):
        self.lines = lines
        self.ortho = ortho

    def vent_count_grid(self):
        max_x = max(max(l.start[0], l.end[0]) for l in self.lines)
        max_y = max(max(l.start[1], l.end[1]) for l in self.lines)
        grid = [
            [0 for _ in range(max_y + 1)]
            for _ in range(max_x + 1)
        ]
        for l in self.lines:
            if not l.ortho() and self.ortho:
                continue
            for p in l.points():
                grid[p[1]][p[0]] += 1

        return grid


def parse(fd):
    lines = []
    for line in fd:
        m = re.match('([0-9]+),([0-9]+) -> ([0-9]+),([0-9]+)', line)
        x0, y0, x1, y1 = (int(m.group(i)) for i in (1, 2, 3, 4))
        lines.append(Line((x0, y0), (x1, y1)))
    return lines


def part_n(ortho):
    with open('input.txt') as fd:
        lines = parse(fd)
    h = HydrothermalGrid(lines, ortho)
    g = h.vent_count_grid()
    #for row in g:
    #    print(''.join('.' if c == 0 else str(c) for c in row))
    overlaps = sum(
        1 for row in g for col in row
        if col >= 2
    )
    print('overlaps:', overlaps)



def part_1():
    part_n(True)


def part_2():
    part_n(False)


if __name__ == '__main__':
    part_1()
    part_2()
