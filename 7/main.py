
def load_horiz():
    with open('input.txt') as fd:
        content = fd.read()
    return [int(i) for i in content.split(',')]


def part_2():
    pos = load_horiz()
    min_pos = min(pos)
    max_pos = max(pos)

    costs = {}
    for c in range(min_pos, max_pos + 1):
        costs[c] = sum(
            abs(c - p) * (abs(c - p) + 1) * 0.5 for p in pos
        )

    print('min cost:', min(costs.values()))

if __name__ == '__main__':
    part_2()
