def get_words():
    words = []
    with open('input.txt') as f:
        for line in f:
            words.append(line.strip())
    return words

def get_streams(words):
    n_streams = len(words[0])
    return [
        [int(w[i]) for w in words]
        for i in range(n_streams)
    ]


def part_1():
    words = get_words()
    streams = get_streams(words)
    most_common = []
    least_common = []
    for stream in streams:
        total = sum(stream)
        if total > len(stream) / 2:
            most_common.append('1')
            least_common.append('0')
        else:
            most_common.append('0')
            least_common.append('1')

    gamma_rate = ''.join(most_common)
    print(gamma_rate)
    gamma_rate = int(gamma_rate, 2)
    eps_rate =  ''.join(least_common)
    print(eps_rate)
    eps_rate = int(eps_rate, 2)
    print('gamma:', gamma_rate)
    print('eps:', eps_rate)
    print('product:', eps_rate * gamma_rate)


def part_2():
    words = get_words()
    streams = get_streams(words)

    # To find oxygen generator rating, determine the most common value (0 or 1)
    # in the current bit position, and keep only numbers with that bit in that
    # position. If 0 and 1 are equally common, keep values with a 1 in the
    # position being considered.
    oxy_words = words
    for bit_pos, stream in enumerate(streams):
        oxy_stream = get_streams(oxy_words)[bit_pos]
        want_ones = sum(oxy_stream) >= len(oxy_stream) / 2
        tgt = '1' if want_ones else '0'
        oxy_words = [w for w in oxy_words if w[bit_pos] == tgt]
        if len(oxy_words) == 1:
            break

    oxy_rating = int(oxy_words[0], 2)
    print('oxy rating:', oxy_rating)


    # To find CO2 scrubber rating, determine the least common value (0 or 1)
    # in the current bit position, and keep only numbers with that bit in that
    # position. If 0 and 1 are equally common, keep values with a 0 in the
    # position being considered.
    co2_words = words
    for bit_pos, stream in enumerate(streams):
        co2_stream = get_streams(co2_words)[bit_pos]
        want_ones = sum(co2_stream) < len(co2_stream) / 2
        tgt = '1' if want_ones else '0'
        co2_words = [w for w in co2_words if w[bit_pos] == tgt]
        if len(co2_words) == 1:
            break

    co2_rating = int(co2_words[0], 2)
    print('co2 rating:', co2_rating)
    print('life support rating:', co2_rating * oxy_rating)


if __name__ == '__main__':
    part_1()
    part_2()
