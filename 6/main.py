from typing import List


class Fish:

    def __init__(self, age):
        self.age = age

    def day(self):
        self.age -= 1
        if self.age < 0:
            self.age = 6
            return [Fish(8)]
        return []

    def __eq__(self, other):
        return self.age == other.age


class LanternfishSchool:

    def __init__(self, ages: List[int]):
        self.school = {
            age: len([1 for a in ages if a == age])
            for age in range(9)
        }

    def day(self):
        zeroes = self.school[0]
        for age in range(0, 8):
            self.school[age] = self.school[age + 1]
        self.school[8] = zeroes
        self.school[6] += zeroes

    def count(self):
        return sum(self.school.values())


def input():
    with open('input.txt') as fd:
        ages = fd.readline()
    return [int(i) for i in ages.split(',')]


def part_1():
    l = LanternfishSchool(input())
    for _ in range(80):
        l.day()

    print('after 80 days: {} fish'.format(l.count()))


def part_2():
    l = LanternfishSchool(input())
    start_len = l.count()
    for i in range(256):
        l.day()
        print('{},{}'.format(i, l.count()))

    print('after 256 days: {} fish'.format(l.count()))


if __name__ == '__main__':
    part_1()
    part_2()
