
class Octogrid:

    def __init__(self, grid):
        self.grid = grid
        self.xlen = len(grid[0])
        self.ylen = len(grid)

    @classmethod
    def from_file(cls, fd):
        return cls([[int(c) for c in row.strip()] for row in fd])

    def step(self):
        # first, all octos increase level by 1
        for x in range(self.xlen):
            for y in range(self.ylen):
                self.grid[y][x] += 1

        # then all octos 9 or above flash
        to_flash = set((x, y) for y in range(self.ylen) for x in range(self.xlen)
                       if self.grid[y][x] > 9)
        flashed = set()
        while len(to_flash) > 0:
            pending = set()
            # iterate over each octo above 9
            for octo in to_flash:
                # perform the flash (increment all neighbours)
                for nx, ny in self.neighbours(*octo):
                    self.grid[ny][nx] += 1
                    # if it is above energy level 9 we MAY want to flash it
                    if self.grid[ny][nx] > 9:
                        pending.add((nx, ny))
                # record that we flashed this octo
                flashed.add(octo)
            # if it is above 9 (pending) and we havent flashed it (-flashed),
            # then we want to flash it
            to_flash = pending - flashed

        # reset all flashed octos to 0
        for x, y in flashed:
            self.grid[y][x] = 0

        return flashed

    def print(self):
        print('\n'.join(
            ''.join(str(o) for o in row)
            for row in self.grid
        ))

    def neighbours(self, x,  y):
        neighbours = [
            (x - 1, y - 1),
            (x - 1, y),
            (x - 1, y + 1),
            (x, y - 1),
            (x, y),
            (x, y + 1),
            (x + 1, y - 1),
            (x + 1, y),
            (x + 1, y + 1),
        ]
        to_remove = set()
        for n in neighbours:
            if n[0] < 0 or n[1] < 0:
                to_remove.add(n)
            if n[0] >= self.xlen or n[1] >= self.xlen:
                to_remove.add(n)
        for t in to_remove:
            neighbours.remove(t)
        return neighbours


def part_1(debug=False):
    with open('input.txt') as fd:
        o = Octogrid.from_file(fd)

    if debug:
        print('before any steps')
        o.print()

    acc = 0
    steps = 100
    for i in range(steps):
        f = o.step()
        acc += len(f)
        if debug:
            print('step {}, {} flashed'.format(i+1, len(f)))
            o.print()

    print('{} steps, {} flashes'.format(steps, acc))


def part_2():
    with open('input.txt') as fd:
        o = Octogrid.from_file(fd)

    steps = 0
    tgt = o.xlen * o.ylen
    while True:
        steps += 1
        flashes = o.step()
        if len(flashes) == tgt:
            break

    print('{} steps, synchronised'.format(steps))


if __name__ == '__main__':
    part_1()
    part_2()
