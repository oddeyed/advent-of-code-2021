from typing import Dict, Set


class Cave:

    def __init__(self, name):
        self.name = name
        self.neighbours = set()

    def add_neighbour(self, c: 'Cave'):
        self.neighbours.add(c)

    @property
    def big(self):
        return self.name.isupper() and self.name not in ('start', 'end')

    @property
    def small(self):
        return not self.big and self.name not in ('start', 'end')

    def __repr__(self):
        return self.name


def load():
    caves = {}
    with open('input.txt') as fd:
        for line in fd:
            lcave, rcave = line.strip().split('-')
            caves.setdefault(lcave, Cave(lcave)).add_neighbour(
                caves.setdefault(rcave, Cave(rcave))
            )
            caves[rcave].add_neighbour(caves[lcave])

    return caves['start']


def explore(cave: Cave, excluded: Set[Cave]):
    """Return all the routes to the end from here"""
    # if we have reached the end, stop
    if cave.name == 'end':
        return [[cave]]

    # don't visit small caves twice
    if cave.small:
        excluded = excluded | { cave }
    # all the adjacents we can go to
    adjacents = [n for n in cave.neighbours if n not in excluded]

    routes = []
    for a in adjacents:
        for route in explore(a, excluded):
            routes.append([cave, *route])
    return routes


def explore2(cave: Cave, visited: Dict[Cave, int], max_small: int = 2):
    """Return all the routes to the end from here"""
    # if we have reached the end, stop
    if cave.name == 'end':
        return [[cave]]

    visited = visited.copy()

    # don't visit small caves more than once
    if cave.small:
        cur = visited.setdefault(cave, 0)
        visited[cave] = cur + 1
        if visited[cave] == 2:
            # this is the second time we visited the cave
            max_small = 1

    excluded = set(cave for cave, count in visited.items() if count >= max_small)

    # all the adjacents we can go to
    adjacents = [n for n in cave.neighbours if n not in excluded]

    routes = []
    for a in adjacents:
        for route in explore2(a, visited, max_small):
            routes.append([cave, *route])
    return routes



def part_1():
    start = load()
    #import pdb; pdb.set_trace()
    routes = explore(start, {start})
    print(len(routes), 'routes from start to end')


def part_2():
    start = load()
    routes = explore2(start, {start: 2})
    routestrings = [','.join([c.name for c in caves]) for caves in routes]
    for r in sorted(routestrings):
        print(r)
    print(len(routes), 'routes from start to end')


if __name__ == '__main__':
    part_1()
    part_2()