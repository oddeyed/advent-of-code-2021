
from os import read


class BingoBoard:

    def __init__(self, block):
        self.rows = [
            [int(col) for col in row.strip().split()]
            for row in block.split('\n')
        ]
        self.width = len(self.rows[0])
        self.height = len(self.rows)
        self.hit = []
        self.last_called = None

    def mark(self, num):
        for idx, row in enumerate(self.rows):
            for jdx, col in enumerate(row):
                if col == num:
                    self.hit.append((idx, jdx))
        self.last_called = num

    def line(self):
        for idx in range(self.height):
            whole_row = [(idx, i) for i in range(self.height)]
            if all(x in self.hit for x in whole_row):
                return True

        for jdx in range(self.width):
            whole_col = [(i, jdx) for i in range(self.width)]
            if all(x in self.hit for x in whole_col):
                return True

        return False

    def layout(self):
        output = []
        for idx, row in enumerate(self.rows):
            outrow = []
            for jdx, col in enumerate(row):
                if (idx, jdx) in self.hit:
                    outrow.append('--')
                else:
                    outrow.append(str(col).rjust(2, ' '))
            output.append(' '.join(outrow))
        return '\n'.join(output)

    def score(self):
        acc = 0
        for idx, row in enumerate(self.rows):
            for jdx, col in enumerate(row):
                if (idx, jdx) not in self.hit:
                    acc += col

        return acc * self.last_called


def read_board(fd):
    out = []
    line = fd.readline().strip()
    while line != '':
        out.append(line)
        line = fd.readline().strip()
    return '\n'.join(out)


def parse(fd):
    draw = [int(i) for i in fd.readline().strip().split(',')]
    assert fd.readline().strip() == ''

    plays = []
    board = read_board(fd)
    while board:
        plays.append(BingoBoard(board))
        board = read_board(fd)

    return draw, plays


def part_1():
    with open('input.txt') as fd:
        draw, boards = parse(fd)

    for d in draw:
        for b in boards:
            b.mark(d)
            if b.line():
                print('winning board')
                print(b.layout())
                print('score:', b.score())
                return

    raise ValueError('no winner??')


def part_2():
    with open('input.txt') as fd:
        draw, boards = parse(fd)

    for d in draw:
        to_remove = []
        for b in boards:
            b.mark(d)
            if b.line():
                to_remove.append(b)

                if len(boards) == 1:
                    print('losing board')
                    print(b.layout())
                    print('score:', b.score())
                    return

        for r in to_remove:
            boards.remove(r)

    raise ValueError('no loser??')


if __name__ == '__main__':
    part_1()
    part_2()
