
from collections import deque
from enum import Enum


class Action(Enum):
    push = 0
    pop = 1


pairs = (
    ('{', '}'),
    ('<', '>'),
    ('(', ')'),
    ('[', ']'),
)
matches = {a: b for a, b in pairs} | {b: a for a, b in pairs}
actions = {a: Action.push for a, _ in pairs} | {b: Action.pop for _, b in pairs}
points = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137,
}

def check(line):
    state = deque()
    for char in line:
        action = actions[char]
        if action == Action.push:
            state.append(char)
        elif action == Action.pop:
            got = state.pop()
            if got != matches[char]:
                return char

    return None


def complete(line):
    state = deque()
    for char in line:
        action = actions[char]
        if action == Action.push:
            state.append(matches[char])
        elif action == Action.pop:
            got = state.pop()
            assert got == char

    return ''.join(reversed(state))


def score(line):
    err = check(line)
    if err:
        return points[err]
    else:
        return 0


completion_points = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4
}


def completion_score(completion):
    acc = 0
    for char in completion:
        acc *= 5
        acc += completion_points[char]
    return acc

def part_1():
    acc = 0
    with open('input.txt') as f:
        for line in f:
            line = line.strip()
            lscore = score(line)
            print(lscore)
            acc += lscore
    print('total:', acc)


def part_2():
    uncorrupted = []
    with open('input.txt') as f:
        for line in f:
            line = line.strip()
            if check(line) is None:
                uncorrupted.append(line)

    scores = []
    for line in uncorrupted:
        completion = complete(line)
        scores.append(completion_score(completion))
        print(completion, scores[-1])

    scores.sort()
    print('middle score:', scores[(len(scores))//2])


if __name__ == '__main__':
    part_1()
    part_2()
