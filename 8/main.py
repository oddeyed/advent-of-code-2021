import itertools
from typing import List, Optional


class Reading:

    def __init__(self, signals, outputs):
        self.signals = signals
        self.outputs = outputs

    @classmethod
    def from_line(cls, line):
        pattern, output = line.split('|')
        signals = tuple(frozenset(d for d in p) for p in pattern.split())
        outputs = tuple(frozenset(d for d in p) for p in output.split())
        return cls(signals, outputs)



class Translator:

    digits_str = [
        'ABCEFG',
        'CF',
        'ACDEG',
        'ACDFG',
        'BCDF',
        'ABDFG',
        'ABDEFG',
        'ACF',
        'ABCDEFG',
        'ABCDFG'
    ]

    digits = [set(x for x in s) for s in digits_str]

    def __init__(self, mapping):
        self.mapping = mapping

    def convert(self, enc_digit: set) -> Optional[int]:
        output = set(self.mapping[i] for i in enc_digit)
        try:
            return self.digits.index(output)
        except ValueError:
            return None

    def output(self, output: List[set]):
        return (
            1000 * self.convert(output[0]) +
            100 * self.convert(output[1]) +
            10 * self.convert(output[2]) +
            1 * self.convert(output[3])
        )


def all_mappings():
    inputs = 'abcdefg'
    outputs = 'ABCDEFG'
    for output in itertools.product(outputs, repeat=len(inputs)):
        mapping = {i: j for i, j in zip(inputs, output)}
        if len(set(mapping.values())) < len(mapping.values()):
            continue
        else:
            yield mapping


def parse():
    with open('input.txt') as f:
        return [Reading.from_line(line) for line in f]


def part_1():
    readings = parse()
    n_1478 = sum(
        sum(1 for o in r.outputs if len(o) in (2, 4, 3, 7))
        for r in readings
    )
    print('1/4/7/8 occurs {} times'.format(n_1478))


def part_2():
    readings = parse()
    acc = 0
    for r in readings:
        valid_mapping = None
        for mapping in all_mappings():
            mapping_failed = False
            t = Translator(mapping)
            if any(t.convert(p) is None for p in r.signals):
                mapping_failed = True
            if any(t.convert(o) is None for o in r.outputs):
                mapping_failed = True

            if mapping_failed:
                continue
            else:
                valid_mapping = mapping
                break

        if not valid_mapping:
            raise ValueError('no valid mapping found')

        t = Translator(valid_mapping)
        output = t.output(r.outputs)
        acc += output
        print('output:', str(output).zfill(4))
    print('sum of outputs:', acc)

if __name__ == '__main__':
    part_1()
    part_2()
